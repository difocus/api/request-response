# Request
Request instantiation
```php
$request = new \ShopExpress\RequestResponse\Request\Request('http://example.com/');
```
Retrieve single input from the query string
```php
$request->query('test', 0);
```
Retrieve all query string data
```php
$request->query();
```
Retrieve single input from request payload
```php
$request->request('test', 0);
```
Retrieve all request payload data
```php
$request->request();
```
Request method
```php
$request->getMethod();
```
## Retrieve an input item from the request
The `input` method retrieves values from entire request payload (including the query string). For the case of GET, HEAD request methods, request body isn't being considered.
```php
$request->input('test', 0);
```
## Determining if an input value is present
If you would like to determine if a value is present on the request and is not empty, you may use the `filled` method. For the case of GET, HEAD request methods, request body isn't being considered.
```php
$request->filled('test');
```
# Response
Response instantiation
```php
$response = new \ShopExpress\RequestResponse\Response\Response(
    Request $request,
    200, 
    ['Content-Type' => 'text/html; charset=utf-8'], 
    'OK'
);
```
Set response body
```php
$response->setBody('test');
```
Retrieve header value
```php
$response->getHeader('Content-Type');
```
## Add or replace header value
If you would like to add header value, pass `true` in the last argument
```php
$response->withHeader('Access-Control-Allow-Methods', 'PUT', true);
$response->withHeader('Access-Control-Allow-Origin', '*');
```