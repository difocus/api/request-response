<?php


namespace ShopExpress\RequestResponse\Exception;


use Exception;
use Throwable;

class InvalidRequestException extends Exception
{
    public function __construct(string $request, Throwable $previous = null)
    {
        $message = sprintf(
            'Request payload %s has an invalid encoding, and can\'t be handled.',
            $request
        );
        parent::__construct($message, 0, $previous);
    }
}