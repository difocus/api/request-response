<?php


namespace ShopExpress\RequestResponse\Request;


use ShopExpress\RequestResponse\Exception\InvalidRequestException;

/**
 * Class Request
 * @package ShopExpress\RequestResponse\Request
 */
class Request
{
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $requestUri;
    /**
     * @var string
     */
    private $pathInfo;
    /**
     * @var array
     */
    private $get;
    /**
     * @var array
     */
    private $request;
    /**
     * @var int|mixed
     */
    private $paginationLimit;
    /**
     * @var int|mixed
     */
    private $paginationStart;
    /**
     * @var mixed|string|null
     */
    private $service = '';
    /**
     * @var int|mixed|null
     */
    private $id = 0;
    /**
     * @var mixed|string|null
     */
    private $field = '';
    /**
     * @var array
     */
    private $server;
    /**
     * @var array
     */
    private $headers;
    /**
     * @var string|null
     */
    private $content;
    /**
     * @var array|null
     */
    private $query;
    /**
     * @var array|null
     */
    private $cookies;
    /**
     * @var array
     */
    private $files;
    /**
     * @var string
     */
    private $startUrl;

    /**
     * Request constructor.
     *
     * @param string|null $startUrl
     * @param string|null $content
     * @param array $get
     * @param array $server
     * @param array $request
     * @param array $cookies
     * @param array $files
     * @param string|null $method
     * @param array $headers
     *
     * @throws InvalidRequestException
     */
    public function __construct(
        ?string $startUrl = null,
        ?string $content = null,
        array $get = [],
        array $server = [],
        array $request = [],
        array $cookies = [],
        array $files = [],
        string $method = null,
        array $headers = []
    ) {
        $this->headers = $headers;
        $this->server = $server;
        $this->get = $get;
        $this->request = $request;
        $this->content = $this->prepareContent($content);
        $this->cookies = $cookies;
        $this->files = $this->prepareFiles($files);
        $this->startUrl = $startUrl;

        $this->method = $method === null ? strtoupper($this->getServerVar('REQUEST_METHOD', 'GET')) : strtoupper($method);

        if (!$this->hasServerVar('REQUEST_URI')) {
            $this->setServerVar('REQUEST_URI', $startUrl);
        }

        $this->requestUri = $this->prepareRequestUri();
        $this->query = $this->prepareQuery($this->hasServerVar('REQUEST_URI') ? $this->getServerVar('REQUEST_URI') : $this->requestUri);

        if ($startUrl) {
            $startUrl = rtrim($startUrl, '/') . '/';
            $urlParts = parse_url(str_replace($startUrl, '', $this->getRequestUri()));
            $serviceParts = explode('/', $urlParts['path']);

            $this->service = $serviceParts[0] ?? null;
            $this->id = $serviceParts[1] ?? null;
            $this->field = $serviceParts[2] ?? null;
        }

        $this->paginationStart = $this->get['start'] ?? 0;
        $this->paginationLimit = $this->get['limit'] ?? 30;
    }

    /**
     * Adds header to request headers list
     *
     * @param string $header
     * @param string $value
     */
    public function withHeader($header, $value): void
    {
        $this->headers[$header] = $value;
    }

    /**
     * Returns full url (apiUrl + query)
     *
     * @return string
     */
    public function getStartUrl(): string
    {
        return $this->startUrl;
    }

    /**
     * @param string $startUrl
     */
    public function setStartUrl(string $startUrl): void
    {
        $this->startUrl = $startUrl;
    }

    /**
     * заполняем массив из query
     *
     * @param string $uri
     *
     * @return array
     */
    protected function prepareQuery(string $uri): array
    {
        $components = parse_url($uri);
        if (isset($components['query'])) {
            parse_str(html_entity_decode($components['query']), $qs);
            return $qs;
        }
        return [];
    }

    /**
     * @return string
     */
    protected function prepareRequestUri(): string
    {
        $requestUri = '';

        if ($this->hasServerVar('REQUEST_URI')) {
            $requestUri = $this->getServerVar('REQUEST_URI');

            if ('' !== $requestUri && isset($requestUri[0]) && '/' === $requestUri[0]) {
                // To only use path and query remove the fragment.
                if (false !== $pos = strpos($requestUri, '#')) {
                    $requestUri = substr($requestUri, 0, $pos);
                }
            } else {
                // HTTP proxy reqs setup request URI with scheme and host [and port] + the URL path,
                // only use URL path.
                $uriComponents = parse_url($requestUri);

                if (isset($uriComponents['path'])) {
                    $requestUri = $uriComponents['path'];
                }

                if (isset($uriComponents['query'])) {
                    $requestUri .= '?' . $uriComponents['query'];
                }
            }
        } elseif ($this->hasServerVar('ORIG_PATH_INFO')) {
            // IIS 5.0, PHP as CGI
            $requestUri = $this->getServerVar('ORIG_PATH_INFO');
            if ('' != $this->getServerVar('QUERY_STRING')) {
                $requestUri .= '?' . $this->getServerVar('QUERY_STRING');
            }
            $this->removeServerVar('ORIG_PATH_INFO');
        }

        // normalize the request URI to ease creating sub-requests from this request
        $this->setServerVar('REQUEST_URI', $requestUri);

        return $requestUri;
    }

    /**
     * Prepares the path info.
     *
     * @return string path info
     */
    protected function preparePathInfo(): string
    {
        if (null === ($requestUri = $this->getRequestUri())) {
            return '/';
        }

        // Remove the query string from REQUEST_URI
        if (false !== $pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }
        if ('' !== $requestUri && '/' !== $requestUri[0]) {
            $requestUri = '/' . $requestUri;
        }

        if (null === ($baseUrl = $this->getStartUrl())) {
            return $requestUri;
        }

        $pathInfo = substr($requestUri, \strlen($baseUrl));
        if (false === $pathInfo || '' === $pathInfo) {
            // If substr() returns false then PATH_INFO is set to an empty string
            return '/';
        }

        return (string)$pathInfo;
    }

    /**
     * Gets the HTTP headers.
     *
     * @return array
     */
    public function getHeaders(): array
    {
        if ($this->headers) {
            return $this->headers;
        }
        $headers = [];
        $contentHeaders = ['CONTENT_LENGTH' => true, 'CONTENT_MD5' => true, 'CONTENT_TYPE' => true];
        foreach ($this->server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            } // CONTENT_* are not prefixed with HTTP_
            elseif (isset($contentHeaders[$key])) {
                $headers[$key] = $value;
            }
        }

        if (isset($this->server['PHP_AUTH_USER'])) {
            $headers['PHP_AUTH_USER'] = $this->server['PHP_AUTH_USER'];
            $headers['PHP_AUTH_PW'] = $this->server['PHP_AUTH_PW'] ?? '';
        } else {
            /*
             * php-cgi under Apache does not pass HTTP Basic user/pass to PHP by default
             * For this workaround to work, add these lines to your .htaccess file:
             * RewriteCond %{HTTP:Authorization} ^(.+)$
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             *
             * A sample .htaccess file:
             * RewriteEngine On
             * RewriteCond %{HTTP:Authorization} ^(.+)$
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteRule ^(.*)$ app.php [QSA,L]
             */

            $authorizationHeader = null;
            if (isset($this->server['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $this->server['HTTP_AUTHORIZATION'];
            } elseif (isset($this->server['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $this->server['REDIRECT_HTTP_AUTHORIZATION'];
            }

            if (null !== $authorizationHeader) {
                if (0 === stripos($authorizationHeader, 'basic ')) {
                    // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)), 2);
                    if (2 == count($exploded)) {
                        [$headers['PHP_AUTH_USER'], $headers['PHP_AUTH_PW']] = $exploded;
                    }
                } elseif (empty($this->server['PHP_AUTH_DIGEST']) && (0 === stripos($authorizationHeader, 'digest '))) {
                    // In some circumstances PHP_AUTH_DIGEST needs to be set
                    $headers['PHP_AUTH_DIGEST'] = $authorizationHeader;
                    $this->server['PHP_AUTH_DIGEST'] = $authorizationHeader;
                } elseif (0 === stripos($authorizationHeader, 'bearer ')) {
                    /*
                     * XXX: Since there is no PHP_AUTH_BEARER in PHP predefined variables,
                     *      I'll just set $headers['AUTHORIZATION'] here.
                     *      http://php.net/manual/en/reserved.variables.server.php
                     */
                    $headers['AUTHORIZATION'] = $authorizationHeader;
                }
            }
        }

        if (isset($headers['AUTHORIZATION'])) {
            return $headers;
        }

        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset($headers['PHP_AUTH_USER'])) {
            $headers['AUTHORIZATION'] = 'Basic ' . base64_encode($headers['PHP_AUTH_USER'] . ':' . $headers['PHP_AUTH_PW']);
        } elseif (isset($headers['PHP_AUTH_DIGEST'])) {
            $headers['AUTHORIZATION'] = $headers['PHP_AUTH_DIGEST'];
        }
        $this->headers = $headers;

        return $headers;
    }

    /**
     * @param string $key
     * @param null $default
     *
     * @return string|null
     */
    public function getHeader(string $key, $default = null): ?string
    {
        if (!$this->headers) {
            $this->getHeaders();
        }
        return $this->headers[$key] ?? $default;
    }

    /**
     * @param string|null $key
     * @param $default
     *
     * @return array|string|null
     */
    public function getServerVar(?string $key, $default = null)
    {
        if ($key === null) {
            return $this->server;
        }

        return $this->server[$key] ?? $default;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasServerVar(string $key): bool
    {
        return array_key_exists($key, $this->server);
    }

    /**
     * @param string $key
     */
    public function removeServerVar(string $key): void
    {
        unset($this->server[$key]);
    }

    /**
     * @param string $key
     * @param $value
     */
    public function setServerVar(string $key, $value): void
    {
        $this->server[$key] = $value;
    }

    /**
     * @param string $startUrl
     *
     * @throws InvalidRequestException
     * @return Request
     */
    public static function createFromGlobals(string $startUrl = null): Request
    {
        return new static($startUrl, null, $_GET, $_SERVER, $_POST, $_COOKIE, $_FILES);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->input($name);
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param $name
     */
    public function __isset($name)
    {
    }

    /**
     * @param array|null $source
     * @param string|null $index
     * @param mixed $default
     *
     * @return mixed
     */
    protected function retrieveItem($source, ?string $index = null, $default = null)
    {
        if ($source === null) {
            if ($index === null) {
                return [];
            }
            return $default;
        }

        if ($index === null) {
            return $source;
        }

        if (isset($source[$index])) {
            $value = $source[$index];
            if (is_string($value)) {
                return strip_tags($value);
            }

            return $value;
        }
        return $default;
    }

    /**
     * alias for input function
     *
     * @param string|null $index
     * @param $value
     *
     * @return mixed
     */
    public function get(?string $index = null, $value = null)
    {
        return $this->input($index, $value);
    }

    /**
     * Retrieve from query URL item.
     *
     * @param string|null $index Optional $_GET index
     * @param mixed $default Returned value if given index not exists in $_GET, or value at that index is null
     *
     * @return mixed Returns the $_GET var at the specified index or the whole $_GET array
     */
    public function query(?string $index = null, $default = null)
    {
        return $this->retrieveItem($this->query, $index, $default);
    }

    /**
     * Retrieve from query URL item.
     *
     * @param string|null $index Optional $_GET index
     * @param mixed $default Returned value if given index not exists in $_GET, or value at that index is null
     *
     * @return mixed Returns the $_GET var at the specified index or the whole $_GET array
     */
    public function request(?string $index = null, $default = null)
    {
        return $this->retrieveItem($this->request, $index, $default);
    }

    /**
     * Returns the request body content.
     *
     * @param string|null $index
     * @param null $default
     *
     * @return mixed The request body content or a resource to read the body stream
     */
    public function content(?string $index = null, $default = null)
    {
        return $this->retrieveItem($this->content, $index, $default);
    }

    /**
     * Retrieve a $_COOKIE item.
     *
     * @param string|null $index Optional index in request payload
     * @param mixed $default Returned value if given index not exists in request payload, or value at that index is null
     *
     * @return mixed Returns request payload value at the specified index or the whole payload array
     */
    public function cookie(?string $index = null, $default = null)
    {
        return $this->retrieveItem($this->cookies, $index, $default);
    }

    /**
     * Parses string request payload into array. If necessary, uncompresses gz.
     * Automatically determines if given string is json encoded or represents query string.
     * Returns gz uncompressed source string, if encoding is unknown.
     *
     * @param string|null $content
     *
     * @throws InvalidRequestException
     * @return array|null
     */
    private function prepareContent(?string $content = null): ?array
    {
        $content = $content ?? file_get_contents('php://input');

        if (empty($content)) {
            return null;
        }

        if ($this->getHeader('Content-Encoding') === 'gzip') {
            $content = gzuncompress($content);
        }

        $decodedData = json_decode($content, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            return $decodedData;
        }

        $decodedArray = [];
        parse_str($content, $decodedArray);
        if (!empty($decodedArray)) {
            return $decodedArray;
        }

        throw new InvalidRequestException($content);
    }

    /**
     * @param array $files
     *
     * @return array
     */
    private function prepareFiles(array $files = []): array
    {
        foreach ($files as &$field) {
            if (is_array($field['name'])) {
                foreach ($field = $this->rearrangeFile($field) as $key => $file) {
                    $field[$key] = UploadedFile::createFromArray($file);
                }
            } else {
                $field = UploadedFile::createFromArray($field);
            }
        }

        return $files;
    }

    /**
     * @param $file
     *
     * @return array
     */
    private function rearrangeFile($file): array
    {
        $fileArray = [];
        foreach ($file as $prop => $all) {
            foreach ($all as $key => $value) {
                $fileArray[$key][$prop] = $value;
            }
        }
        return $fileArray;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getField(): ?string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @return int
     */
    public function getPaginationLimit(): int
    {
        return $this->paginationLimit;
    }

    /**
     * @return int
     */
    public function getPaginationStart(): int
    {
        return $this->paginationStart;
    }

    /**
     * Get the input source for the request.
     *
     * @return array
     */
    protected function getInputSource(): array
    {
        return in_array($this->getMethod(), ['GET', 'HEAD']) ? $this->get : ($this->request + ($this->content ?? []));
    }

    /**
     * Retrieve an input item from the request.
     *
     * @param string|null $key
     * @param mixed $default
     *
     * @return mixed
     */
    protected function input(?string $key = null, $default = null)
    {
        $input = $this->getInputSource() + $this->get + $this->query;
        return $this->retrieveItem($input, $key, $default);
    }

    /**
     * Determine if the request contains a non-empty value for an input item.
     *
     * @param array|mixed $key
     *
     * @return bool
     */
    public function filled($key): bool
    {
        $keys = is_array($key) ? $key : func_get_args();
        foreach ($keys as $key) {
            if ($this->isEmptyString($key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determine if the given input key is an empty string.
     *
     * @param string $key
     *
     * @return bool
     */
    protected function isEmptyString(string $key): bool
    {
        $value = $this->input($key);

        return !is_bool($value) && !is_array($value) && trim((string)$value) === '';
    }

    /**
     * Get a subset containing the provided keys with values from the input data.
     *
     * @param array|mixed $keys
     *
     * @return array
     */
    public function only($keys): array
    {
        $keys = is_array($keys) ? $keys : func_get_args();
        $results = [];
        $input = $this->input();

        foreach ($keys as $key) {
            if (array_key_exists($key, $input)) {
                $results[$key] = $input[$key];
            }
        }
        return $results;
    }

    /**
     * Get all of the input except for a specified array of items.
     *
     * @param array|mixed $keys
     *
     * @return array
     */
    public function except($keys): array
    {
        $keys = is_array($keys) ? $keys : func_get_args();
        $results = $this->input();

        foreach ($keys as $key) {
            if (array_key_exists($key, $results)) {
                unset($results[$key]);
            }
        }
        return $results;
    }

    /**
     * @return mixed
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }

    /**
     * @return string
     */
    public function getPathInfo(): string
    {
        if (null === $this->pathInfo) {
            $this->pathInfo = $this->preparePathInfo();
        }

        return $this->pathInfo;
    }

    /**
     * @return bool
     */
    public function isXmlHttpRequest(): bool
    {
        return 'XMLHttpRequest' == $this->getServerVar('HTTP_X_REQUESTED_WITH');
    }

    /**
     * @return string|null
     */
    public function getClientIp(): ?string
    {
        foreach (
            [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ] as $key
        ) {
            if (array_key_exists($key, $this->server)) {
                foreach (explode(',', $this->server[$key]) as $ip) {
                    $ip = trim($ip);
                    if ((bool)filter_var($ip, FILTER_VALIDATE_IP,
                        FILTER_FLAG_IPV4 |
                        FILTER_FLAG_IPV6 |
                        FILTER_FLAG_NO_PRIV_RANGE |
                        FILTER_FLAG_NO_RES_RANGE)
                    ) {
                        return $ip;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     * @return void
     */
    public function setFiles(array $files): void
    {
        $this->files = $files;
    }
}