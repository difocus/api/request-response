<?php


namespace ShopExpress\RequestResponse\Request;


use RuntimeException;

/**
 * Class UploadedFile
 * @package ShopExpress\RequestResponse\Request
 */
class UploadedFile
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $mimeType;
    /**
     * @var string
     */
    private $tmpPath;
    /**
     * @var int
     */
    private $size;
    /**
     * @var int
     */
    private $error;

    /**
     * UploadedFile constructor.
     *
     * @param string $name
     * @param string $mimeType
     * @param string $tmpPath
     * @param int $size
     * @param int $error
     */
    public function __construct(string $name, string $mimeType, string $tmpPath, int $size, int $error = 0)
    {
        $this->name = $this->prepareName($name);
        $this->mimeType = $mimeType ?: 'application/octet-stream';
        $this->tmpPath = $tmpPath;
        $this->size = $size;
        $this->error = $error ?: UPLOAD_ERR_OK;
    }

    /**
     * @param array $array
     *
     * @return static
     */
    public static function createFromArray(array $array): self
    {
        return new static($array['name'], $array['type'], $array['tmp_name'], $array['size'], $array['error']);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTmpPath(): string
    {
        return $this->tmpPath;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * Returns locale independent base name of the given path.
     *
     * @param string $name The new file name
     *
     * @return string containing
     */
    private function prepareName($name): string
    {
        $originalName = str_replace('\\', '/', $name);
        $pos = strrpos($originalName, '/');
        $originalName = false === $pos ? $originalName : substr($originalName, $pos + 1);

        return $originalName;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * Returns the original file extension.
     *
     * It is extracted from the original file name that was uploaded.
     * Then it should not be considered as a safe value.
     *
     * @return string The extension
     */
    public function getClientOriginalExtension(): string
    {
        return pathinfo($this->name, PATHINFO_EXTENSION);
    }

    /**
     * Returns whether the file was uploaded successfully.
     *
     * @return bool True if the file has been uploaded with HTTP and no error occurred
     */
    public function isValid(): bool
    {
        $isOk = UPLOAD_ERR_OK === $this->error;

        return $isOk && is_uploaded_file($this->getTmpPath());
    }

    /**
     * Moves the file to a new location.
     *
     * @param string $directory The destination folder
     * @param string $name      The new file name
     *
     * @return string
     *
     */
    public function move(string $directory, string $name = null): string
    {
        if ($this->isValid()) {
            $target = $directory . ($name ?? $this->getName());

            set_error_handler(static function ($type, $msg) use (&$error) {
                $error = $msg;
            });
            $moved = move_uploaded_file($this->getTmpPath(), $target);
            restore_error_handler();

            if (!$moved) {
                throw new RuntimeException(sprintf('Could not move the file "%s" to "%s" (%s)', $this->getTmpPath(), $target, strip_tags($error)));
            }

            @chmod($target, 0666 & ~umask());

            return $target;
        }

        throw new RuntimeException($this->getErrorMessage());
    }

    /**
     * Returns an informative upload error message.
     *
     * @return string The error message regarding the specified error code
     */
    public function getErrorMessage(): string
    {
        static $errors = [
            UPLOAD_ERR_INI_SIZE => 'The file "%s" exceeds your upload_max_filesize ini directive.',
            UPLOAD_ERR_FORM_SIZE => 'The file "%s" exceeds the upload limit defined in your form.',
            UPLOAD_ERR_PARTIAL => 'The file "%s" was only partially uploaded.',
            UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
            UPLOAD_ERR_CANT_WRITE => 'The file "%s" could not be written on disk.',
            UPLOAD_ERR_NO_TMP_DIR => 'File could not be uploaded: missing temporary directory.',
            UPLOAD_ERR_EXTENSION => 'File upload was stopped by a PHP extension.',
        ];

        $errorCode = $this->error;
        $message = $errors[$errorCode] ?? 'The file "%s" was not uploaded due to an unknown error.';

        return sprintf($message, $this->getName());
    }
}
