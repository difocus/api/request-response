<?php

namespace ShopExpress\RequestResponse\Response;

use InvalidArgumentException;
use ShopExpress\RequestResponse\Request\Request;

/**
 * Class Response
 * Encapsulates information about HTTP response
 */
class Response
{
    public const COOKIES_FLAT = 'flat';
    public const COOKIES_ARRAY = 'array';

    /** @var array Map of all registered headers, as original name => array of values */
    private $headers = [];

    /** @var array Map of lowercase header name => original name at registration */
    private $headerNames = [];

    /** @var array|string */
    protected $content;

    /** @var array Map of standard HTTP status code/reason phrases */
    private static $phrases = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    ];

    /** @var string */
    private $reasonPhrase = '';

    /** @var int */
    private $statusCode = 200;

    /** @var string */
    private $charset = 'utf-8';

    /** @var string */
    private $version = '1.0';

    /** @var Cookie[] */
    private $cookies = [];
    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     * @param int $status Status code
     * @param array $headers Response headers
     * @param string|null $reason Reason phrase (when empty a default will be used based on the status code)
     */
    public function __construct(
        Request $request,
        $status = 0,
        array $headers = [],
        $reason = null
    ) {
        if (filter_var($status, FILTER_VALIDATE_INT) === false) {
            throw new InvalidArgumentException('Status code must be an integer value.');
        }
        $this->statusCode = (int)$status;

        $this->setHeaders($headers);
        if ($reason == '' && isset(self::$phrases[$this->statusCode])) {
            $this->reasonPhrase = self::$phrases[$this->statusCode];
        } else {
            $this->reasonPhrase = (string)$reason;
        }
        $this->request = $request;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

    /**
     * @param int $statusCode
     *
     * @return Response
     */
    public function setStatusCode(int $statusCode): Response
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function setCharset(string $charset): Response
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * HTTP request was successful
     *
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->statusCode < 400;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param $format
     *
     * @return array|Cookie[]
     */
    public function getCookies($format = self::COOKIES_FLAT)
    {
        if (!\in_array($format, [self::COOKIES_FLAT, self::COOKIES_ARRAY])) {
            throw new \InvalidArgumentException(sprintf('Format "%s" invalid (%s).', $format, implode(', ', [self::COOKIES_FLAT, self::COOKIES_ARRAY])));
        }

        if (self::COOKIES_ARRAY === $format) {
            return $this->cookies;
        }

        $flattenedCookies = [];
        foreach ($this->cookies as $path) {
            foreach ($path as $cookies) {
                foreach ($cookies as $cookie) {
                    $flattenedCookies[] = $cookie;
                }
            }
        }

        return $flattenedCookies;
    }

    /**
     * @param string $header
     * @return bool
     */
    public function hasHeader(string $header): bool
    {
        return isset($this->headerNames[strtolower($header)]);
    }

    /**
     * Returns header value by its name. If the header doesn't present, returns empty array
     *
     * @param string $header Name of the header
     * @return array
     */
    public function getHeader(string $header)
    {
        $header = strtolower($header);

        if (!isset($this->headerNames[$header])) {
            return [];
        }

        $header = $this->headerNames[$header];

        return $this->headers[$header];
    }

    /**
     * @return array|string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Appends string to body
     *
     * @param mixed $append String to append
     * @return Response Appended string length
     */
    public function appendContent($append): Response
    {
        if (is_array($this->content)) {
            $this->content = array_merge($this->content, $append);
        } else {
            $this->content = $this->content ?? '';
            $this->content .= $append;
        }

        return $this;
    }

    /**
     * @param null $content
     * @return Response
     */
    public function setContent($content): Response
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Adds or replaces header value
     *
     * @param string $header
     * @param mixed $value
     * @param bool $add Whether or not to add header
     *
     * @return Response
     */
    public function withHeader(string $header, $value, bool $add = false): Response
    {
        if (!is_array($value)) {
            $value = [$value];
        }
        $value = $this->trimHeaderValues($value);
        $normalized = strtolower($header);

        if (isset($this->headerNames[$normalized])) {
            if (!$add) {
                unset($this->headers[$this->headerNames[$normalized]]);
            } else {
                $header = $this->headerNames[$normalized];
                $this->headers[$header] = array_merge($this->headers[$header], $value);
            }
        }
        if (!$add || !isset($this->headerNames[$normalized])) {
            $this->headerNames[$normalized] = $header;
            $this->headers[$header] = $value;
        }

        return $this;
    }

    /**
     * @param array $headers
     *
     * @return void
     */
    private function setHeaders(array $headers): void
    {
        $this->headerNames = $this->headers = [];
        foreach ($headers as $header => $value) {
            if (!is_array($value)) {
                $value = [$value];
            }

            $value = $this->trimHeaderValues($value);
            $normalized = strtolower($header);
            if (isset($this->headerNames[$normalized])) {
                $header = $this->headerNames[$normalized];
                $this->headers[$header] = array_merge($this->headers[$header], $value);
            } else {
                $this->headerNames[$normalized] = $header;
                $this->headers[$header] = $value;
            }
        }
    }

    /**
     * Trims whitespace from the header values.
     *
     * Spaces and tabs ought to be excluded by parsers when extracting the field value from a header field.
     *
     * header-field = field-name ":" OWS field-value OWS
     * OWS          = *( SP / HTAB )
     *
     * @param string[] $values Header values
     *
     * @return string[] Trimmed header values
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function trimHeaderValues(array $values)
    {
        return array_map(function ($value) {
            return trim($value, " \t");
        }, $values);
    }

    /**
     * Sends HTTP headers.
     *
     * @return Response
     */
    public function sendHeaders()
    {
        // headers have already been sent by the developer
        if (headers_sent()) {
            return $this;
        }

        // status
        header(sprintf('HTTP/%s %s %s', $this->getVersion(), $this->getStatusCode(), $this->getReasonPhrase()), true, $this->statusCode);

        // headers
        foreach ($this->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header($name . ': ' . $value, false, $this->getStatusCode());
            }
        }

        // cookies
        foreach ($this->getCookies() as $cookie) {
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
        }

        return $this;
    }

    /**
     * Sends HTTP headers and content.
     *
     * @return Response
     *
     * @api
     */
    public function send(): Response
    {
        if (is_array($this->getContent()) && !$this->hasHeader('Content-Type')) {
            $this->withHeader('Content-Type', 'application/json; charset=' . $this->charset);
        }

        $this->sendHeaders();
        $this->sendContent();

        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        } elseif ('cli' !== PHP_SAPI) {
            static::closeOutputBuffers(0, true);
        }

        return $this;
    }

    public function sendContent(): void
    {
        if (is_array($this->getContent())) {
            $content = [
                'result' => $this->getContent(),
            ];

            if ($this->request->getService()) {
                $content['service'] = $this->request->getService();
            }
            if ($this->request->getId()) {
                $content['id'] = $this->request->getId();
            }

            if (!$this->isSuccessful()) {
                $content['error'] = [
                    'code' => $this->getStatusCode(),
                    'message' => $this->getReasonPhrase()
                ];
            }

            echo json_encode($content);
        } else {
            echo $this->getContent();
        }
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * Cleans or flushes output buffers up to target level.
     *
     * Resulting level can be greater than target level if a non-removable buffer has been encountered.
     *
     * @param int $targetLevel The target output buffering level
     * @param bool $flush Whether to flush or clean the buffers
     */
    public static function closeOutputBuffers($targetLevel, $flush)
    {
        $status = ob_get_status(true);
        $level = count($status);
        $flags = defined('PHP_OUTPUT_HANDLER_REMOVABLE') ? PHP_OUTPUT_HANDLER_REMOVABLE | ($flush ? PHP_OUTPUT_HANDLER_FLUSHABLE : PHP_OUTPUT_HANDLER_CLEANABLE) : -1;

        while ($level-- > $targetLevel && ($s = $status[$level]) && (!isset($s['del']) ? !isset($s['flags']) || $flags === ($s['flags'] & $flags) : $s['del'])) {
            if ($flush) {
                ob_end_flush();
            } else {
                ob_end_clean();
            }
        }
    }

    /**
     * @param string $reasonPhrase
     * @return Response
     */
    public function setReasonPhrase(string $reasonPhrase): Response
    {
        $this->reasonPhrase = $reasonPhrase;
        return $this;
    }

    /**
     * @param Cookie[] $cookies
     * @return Response
     */
    public function setCookies(array $cookies): Response
    {
        $this->cookies = [];
        foreach ($cookies as $cookie) {
            $this->addCookie($cookie);
        }
        return $this;
    }

    /**
     * @param Cookie $cookie
     * @return Response
     */
    public function addCookie(Cookie $cookie): Response
    {
        $this->cookies[$cookie->getDomain()][$cookie->getPath()][$cookie->getName()] = $cookie;
        return $this;
    }

    /**
     * Removes a cookie from the array, but does not unset it in the browser.
     *
     * @param string $name
     * @param string $path
     * @param string $domain
     */
    public function removeCookie($name, $path = '/', $domain = null): void
    {
        if (null === $path) {
            $path = '/';
        }

        unset($this->cookies[$domain][$path][$name]);

        if (empty($this->cookies[$domain][$path])) {
            unset($this->cookies[$domain][$path]);

            if (empty($this->cookies[$domain])) {
                unset($this->cookies[$domain]);
            }
        }
    }

    /**
     * Clears a cookie in the browser.
     *
     * @param string $name
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httpOnly
     */
    public function clearCookie($name, $path = '/', $domain = null, $secure = false, $httpOnly = true)
    {
        $this->addCookie(new Cookie($name, null, 1, $path, $domain, $secure, $httpOnly));
    }
}
