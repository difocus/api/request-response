<?php


use PHPUnit\Framework\TestCase;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;

class RequestTest extends TestCase
{
    /**
     * @return Request
     * @throws InvalidRequestException
     */
    public function testValidInit()
    {
        $request = new Request(
            '/adm/api',
            gzcompress(json_encode(['test' => 'content'])),
            [],
            [
                'REQUEST_METHOD' => 'GET',
                'HTTPS' => '1',
                'REQUEST_URI' => '/adm/api/articles/1/field?test=query&field1=value1',
            ],
            [], [], [], null, [
                'Content-Encoding' => 'gzip',
            ]
        );
        $this->assertInstanceOf(Request::class, $request, 'Неа');
        return $request;
    }

    /**
     * @depends testValidInit
     * @param $request
     */
    public function testRequest(Request $request)
    {
        $this->assertEquals('content', $request->content('test'));
        $this->assertEquals('query', $request->get('test'));
    }

    /**
     * @throws InvalidRequestException
     */
    public function testHeaders()
    {
        $request = new ShopExpress\RequestResponse\Request\Request();
        $request->withHeader('Content-type', 'application/json');
        $this->assertEquals('application/json', $request->getHeader('Content-type'));
    }

    /**
     * @throws InvalidRequestException
     */
    public function testUri()
    {
        $request = new ShopExpress\RequestResponse\Request\Request('test.loc', json_encode(['param' => true]));
        $this->assertEquals('test.loc', $request->getStartUrl());
    }

    /**
     * @throws InvalidRequestException
     */
    public function testBody()
    {
        $request = new ShopExpress\RequestResponse\Request\Request('test.loc', json_encode(['test' => 1]));
        $this->assertEquals(json_encode(['test' => 1]), json_encode($request->content()));
    }

    /**
     * @depends testValidInit
     * @param $request
     */
    public function testMethod(Request $request)
    {
        $this->assertEquals($request->getMethod(), 'GET');
    }

    /**
     * @depends testValidInit
     * @param $request
     */
    public function testService(Request $request)
    {
        $this->assertEquals($request->getService(), 'articles');
        $this->assertEquals($request->getId(), '1');
        $this->assertEquals($request->getField(), 'field');
    }

    /**
     * @throws InvalidRequestException
     */
    public function testQueryString()
    {
        $request = new Request(
            '/adm/api',
            null,
            [
                'field2' => 'value2',
            ],
            [
                'REQUEST_METHOD' => 'GET',
                'HTTPS' => '1',
                'REQUEST_URI' => '/adm/api/articles/1/field?test=test&field1=value1'
            ],
            []
        );

        $this->assertEquals($request->get('test'), 'test');
        $this->assertEquals($request->get('field2'), 'value2');
    }

    /**
     * @throws InvalidRequestException
     */
    public function testPostMethod()
    {
        $request = new Request(
            '/adm/api',
            'key1=value1&key2=value2',
            ['key3' => 'value3'],
            [
                'REQUEST_METHOD' => 'POST',
                'HTTPS' => '1',
                'REQUEST_URI' => '/adm/api/articles/1/field?key4=value4&key5=value5'
            ],
            ['key6' => 'value6']
        );

        $this->assertEquals($request->getMethod(), 'POST');

        $this->assertEquals($request->get('key1'), 'value1');
        $this->assertEquals($request->get('key2'), 'value2');
        $this->assertEquals($request->get('key3'), 'value3');
        $this->assertEquals($request->get('key4'), 'value4');
        $this->assertEquals($request->get('key5'), 'value5');

        $this->assertEquals($request->content(), ['key1' => 'value1', 'key2' => 'value2']);
        $this->assertEquals($request->request(), ['key6' => 'value6']);

        $this->assertEquals($request->query(), ['key4' => 'value4', 'key5' => 'value5']);
    }

    /**
     * @throws InvalidRequestException
     */
    public function testCookie()
    {
        $request = new Request('/adm/api', '', [], [], [], ['field3' => 'value3']);

        $this->assertEquals($request->cookie(), ['field3' => 'value3']);
        $this->assertEquals($request->cookie('field3'), 'value3');
    }

    /**
     * @throws InvalidRequestException
     */
    public function testClientIp()
    {
        $request = new Request('/adm/api', '', [], ['HTTP_FORWARDED_FOR' => '2.93.137.123'], [], []);
        $this->assertEquals($request->getClientIp(), '2.93.137.123');

        $request = new Request('/adm/api', '', [], ['HTTP_CLIENT_IP' => '127.93.137.123'], [], []);
        $this->assertEquals($request->getClientIp(), null);

        $request = new Request('/adm/api', '', [], ['HTTP_X_CLUSTER_CLIENT_IP' => '::ffff:c000:027b'], [], []);
        $this->assertEquals($request->getClientIp(), '::ffff:c000:027b');
    }
}