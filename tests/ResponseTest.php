<?php


namespace ShopExpress\RequestResponse\Test;


use PHPUnit\Framework\TestCase;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;
use ShopExpress\RequestResponse\Response\Cookie;
use ShopExpress\RequestResponse\Response\Response;

class ResponseTest extends TestCase
{
    /**
     * @throws InvalidRequestException
     */
    public function testAnswerJson()
    {
        $request = new Request(
            '/adm/api',
            gzcompress(json_encode(['test' => 'test'])),
            [],
            [
                'REQUEST_METHOD' => 'GET',
                'HTTPS' => '1',
                'REQUEST_URI' => '/adm/api/articles/1/field?test=test&field1=value1'
            ]
        );
        $response = new Response(
            $request,
            200, [
            'Content-Type' => 'text/html; charset=utf-8'
        ],  'OK');
        $response->setContent(['data'=>'some text']);

        ob_start();
        $response->send();
        $this->assertEquals(ob_get_contents(), '{"result":{"data":"some text"},"service":"articles","id":"1"}', 'data ответы не совпали');
        ob_end_clean();
    }

    /**
     * @throws InvalidRequestException
     */
    public function testAnswerPlain()
    {
        $request = new Request(
            null,
            gzcompress(json_encode(['test' => 'test'])),
            [],
            [
                'REQUEST_METHOD' => 'GET',
                'HTTPS' => '1',
                'REQUEST_URI' => '/somepage.html'
            ]
        );

        $response = new Response(
            $request,
            200, [
            'Content-Type' => 'text/html; charset=utf-8'
        ],  'OK');
        $response->setContent(['some text']);

        ob_start();
        $response->send();
        $this->assertEquals(ob_get_contents(), '{"result":["some text"]}', 'some text ответы не совпали');
        ob_end_clean();
    }

    public function testCookie()
    {
        $response = new Response(
            new Request(),
            200, [
            'Content-Type' => 'text/html; charset=utf-8',
        ], 'OK'
        );

        $cookie = new Cookie('name', 'value', 1000, '/path/to', 'domain.org');
        $response->addCookie($cookie);

        $cookies = $response->getCookies();
        $this->assertEquals($cookies[0], $cookie);

        $cookies = $response->getCookies(Response::COOKIES_ARRAY);
        $this->assertEquals($cookies[$cookie->getDomain()][$cookie->getPath()][$cookie->getName()], $cookie);

        try {
            $response->getCookies('test');
        } catch (\InvalidArgumentException $e) {
            $this->assertEquals($e->getMessage(), 'Format "test" invalid (flat, array).');
        }

        $response->removeCookie($cookie->getName(), $cookie->getPath(), $cookie->getDomain());

        $this->assertEquals($response->getCookies(), []);

        $response->clearCookie($cookie->getName(), $cookie->getPath(), $cookie->getDomain());

        $cookies = $response->getCookies();
        $this->assertEquals($cookies[0], new Cookie($cookie->getName(), null, 1, $cookie->getPath(), $cookie->getDomain(), false, true));
    }
}